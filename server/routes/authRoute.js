var express = require('express');
var router = express.Router();

var authController = require('../controllers/authController');

router.post('/register', authController.register);

router.post('/login', authController.login);

router.post('/refreshtoken', authController.refreshtoken);

module.exports = router;