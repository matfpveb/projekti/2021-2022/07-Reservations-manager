var express = require('express');
var router = express.Router();

var terrainsController = require('../controllers/terrainsController');

var authadmin = require('../middleware/authadmin');
var auth = require('../middleware/auth');

//auth - any loged in user
//authadmin - only admin user

router.get('/getAll/', auth, terrainsController.getAll);

router.get('/getByTerrainId/:id', auth, terrainsController.getByTerrainId);

router.post('/terrainPost/', authadmin, terrainsController.terrainPost);

router.put('/terrainPut/:id', authadmin, terrainsController.terrainPut);

router.delete('/terrainDelete/:id', authadmin, terrainsController.terrainDelete);

module.exports = router;