var express = require('express');
var router = express.Router();

var reservation_c = require('../controllers/reservationsController');

var authadmin = require('../middleware/authadmin');
var auth = require('../middleware/auth');

router.get('/getAll', reservation_c.getAll);

router.get('/getByTerrainId/:terrainId', auth, reservation_c.getByTerrainId);

router.get('/getByReservationId/:id', reservation_c.getByReservationId);

router.get('/getByUsername/:username', reservation_c.getByUsername);

router.get('/getByUserId/:userId/:all', reservation_c.getByUserId);

router.post('/addReservation', reservation_c.addreservation);

router.post('/addReservation2', reservation_c.addreservation2);

router.delete('/removeReservation/:id', reservation_c.removeReservation);

router.put('/changeReservation/:id', reservation_c.changeReservation);


module.exports = router;