var express = require('express');
var router = express.Router();
const bcrypt = require("bcryptjs");
const userController = require('../controllers/userController');

//Just one function GET/:user_id -> return username
router.get('/:user_id', userController.getById);

router.get('/email/:id/:subject/:body', userController.getMail);

router.get('/', userController.getAll);

router.put('/userPut/:id', userController.putUser);

router.put('/userPutPass/:id/:oldPass/:newPass', userController.putUserPass);

module.exports = router;