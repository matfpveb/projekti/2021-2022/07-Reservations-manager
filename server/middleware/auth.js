const jwt = require("jsonwebtoken");

const config = process.env;

const verifyToken = (req, res, next) => {
    let token =
         req.headers["authorization"];

    token = String(token).substring(7);

    if (!token) {
        return res.status(403).send("Unauthorized");
    }
    try {
        const decoded = jwt.verify(token, config.TOKEN_KEY);
        //prosledjuje decodovanog usera dalje
        //tako sto ga stavlja u req
        //posto je ovo middleware funkcija
        req.user = decoded;
    } catch (err) {
        return res.status(401).send("Invalid Token");
    }
    return next();
};

module.exports = verifyToken;