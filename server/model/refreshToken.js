const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");

//Jwt tokeni imaju rok trajanja.
//Prosledjivanjem refresh jwt tokena sa isteklim jwt tokenom
//dobija se novi jwt token.
//Tj. klijent kojem je istekao jwt token umesto da mora opet
//da se uloguje svaki put kada se to desi ima mogucnost da 
//posalje istekao jwt token + jwt refresh token i za uzvrat 
//dobije novi jwt token i ostane ulogovan.
const refreshTokenSchema = new mongoose.Schema({
    token: String,
    //pravim referencu na korisnika 
    //njemu pripada refresh token
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    expiryDate: Date
});

refreshTokenSchema.statics.createToken = async function(user){
    //Do I need to put Role into a refresh token
    const Token = jwt.sign(
        {user_id: user._id, Email: user.Email},
        process.env.TOKEN_KEY,
        {
            expiresIn: "86400s" //24h
        }
    );

    let expiredAt = new Date();

    expiredAt.setSeconds(
        expiredAt.getSeconds() + 86400
    );

    let object = new this({
        token: Token,
        user: user._id,
        expiryDate: expiredAt.getTime()
    });

    let refreshToken = await object.save();

    return refreshToken.token;
}

refreshTokenSchema.statics.verifyExpiration = (token) => {
    return token.expiryDate.getTime() < new Date().getTime();
}

const refreshToken = mongoose.model("refreshToken", refreshTokenSchema);

module.exports = refreshToken;