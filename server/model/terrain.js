const mongoose = require("mongoose");

const terrainSchema = new mongoose.Schema({
    Name: {type: String, default:null, unique: true},
    Sport: {type: String, default: null},
    Price: {type: Number, default:0}
});



module.exports = mongoose.model("terrain", terrainSchema);
