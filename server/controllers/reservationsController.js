const express = require("express");
const Reservation = require('../model/reservation');
const Terrain = require('../model/terrain');
const User = require('../model/user');


exports.addreservation = async (req, res) => {
    try {
        let { reservationDateTime, Username, Duration, terrainName } = req.body;

        if (!(reservationDateTime && Username && terrainName && Duration)) {
            return res.status(400).send("All fields are required");
        }

        if(Duration <=0){
            return res.status(400).send("Invalid duration");
        }

        //proveri format za vreme pre ovoga
        try {
            reservationDateTime = new Date(reservationDateTime);
            if(reservationDateTime < new Date()){
                return res.status(400).send("Cannot make a reservation in the past");
            }
        }
        catch (err) {
            return res.status(400).send("Invalid date format");
        }

        var endTime = new Date(reservationDateTime);
        endTime.setHours(endTime.getHours() + Duration);

        var overlap = await overlapCheck(reservationDateTime, endTime, terrainId);
        console.log(overlap);
        if(overlap == true){
            return res.status(400).send("Reservation overlapping with existing");
        }

        const terrainExists = await Terrain.findOne({ Name: terrainName });
        const userIdExists = await User.findOne({ Username: Username });
        const reservationExists = await Reservation.findOne({ reservation_date_time: reservationDateTime, _terain_id: terrainExists._id });

        if (reservationExists) {
            return res.status(400).send("Reservation for that hour already exists");
        }

        if (!terrainExists || !userIdExists) {
            return res.status(404).send("An id from request doesn't exist");
        }

        if (reservationDateTime.getHours() < 7 || reservationDateTime.getHours() + Duration > 24) {
            return res.status(400).send("Cannot make a reservation that isn't between 7am and 24pm");
        }

        var calculatePrice = terrainExists.Price * Duration;

        const reservation = await Reservation.create({
            reservation_date_time: reservationDateTime,
            end_date_time: endTime,
            user_id: userIdExists._id,
            username: Username,
            _terrain_id: terrainExists._id,
            price: calculatePrice,
            time_zone: reservationDateTime.getTimezoneOffset(),
            duration: Duration
        }).catch((error) => {
            res.status(500).send(error);
        });

        return res.status(201).json(reservation);

    } catch (err) {
        console.log(err);
    }
}

exports.addreservation2 = async (req, res) => {
    try {
        let { reservationDateTime, Username, Duration, terrainId } = req.body;

        if (!(reservationDateTime && Username && terrainId)) {
            return res.status(400).send("All fields are required");
        }

        if(Duration <=0){
            return res.status(400).send("Invalid duration");
        }

        //proveri format za vreme pre ovoga
        try {
            reservationDateTime = new Date(reservationDateTime);
            if(reservationDateTime < new Date()){
                return res.status(400).send("Cannot make a reservation in the past");
            }
        }
        catch (err) {
            return res.status(400).send("Invalid date format");
        }
        
        var endTime = new Date(reservationDateTime);
        endTime.setHours(endTime.getHours() + Duration);

        var overlap = await overlapCheck(reservationDateTime, endTime, terrainId);
        if(overlap === true){
            return res.status(400).send("Reservation overlapping with existing");
        }

        const terrainExists = await Terrain.findOne({ _id: terrainId });
        const userIdExists = await User.findOne({ Username: Username });
        const reservationExists = await Reservation.findOne({ reservation_date_time: reservationDateTime, _terain_id: terrainExists._id });

        if (reservationExists) {
            return res.status(400).send("Reservation for that hour already exists");
        }

        if (!terrainExists || !userIdExists) {
            return res.status(404).send("An id from request doesn't exist");
        }

        if (reservationDateTime.getHours() < 7 || reservationDateTime.getHours() + Duration > 24) {
            return res.status(400).send("Cannot make a reservation that isn't between 7am and 24pm");
        }

        var calculatePrice = terrainExists.Price * Duration;

        const reservation = await Reservation.create({
            reservation_date_time: reservationDateTime,
            end_date_time: endTime,
            user_id: userIdExists._id,
            username: Username,
            _terrain_id: terrainExists._id,
            price: calculatePrice,
            time_zone: reservationDateTime.getTimezoneOffset(),
            duration: Duration
        }).catch((error) => {
            res.status(500).send(error);
        });

        return res.status(201).json(reservation);

    } catch (err) {
        console.log(err);
    }
}

// DELETE request on url/reservations/removeReservation/:id
// returns deleted object
exports.removeReservation = async (req, res) => {
    Reservation.findByIdAndDelete(req.params.id).then(
        (reservation) => {
            if (!reservation) {
                return res.status(404).send();
            }

            res.send(reservation);
        })
        .catch((error) => {
            res.status(500).send(error);
        });
}

// url/changeReservation/:id
// wih body containing {"_id": "idOfObject", ... fields to change }
//
// checks if id from url is same as url from object 
// then tries to update
// if successful returns a message

//vreme mora da bude tacno u ovom formatu
exports.changeReservation = async (req, res) => {
    const id = req.params.id;
    let reservation = req.body;

    if (id != reservation._id) {
        return res.status(400).send("Id's in url and body don't match");
    } 

    if(reservation.reservation_date_time !== undefined && reservation.end_date_time !== undefined){
        var overlap = await overlapCheck(reservation.reservation_date_time, reservation.end_date_time);
        if(overlap === true){
            return res.status(400).send("Reservation overlapping with existing");
        }
    }

    if (reservation.reservation_date_time !== undefined) {

        if(reservation.reservation_date_time < new Date()){
            return res.status(400).send("Cannot make a reservation in the past");
        }

        var timeZone;
        try {
            timeZone = new Date(reservation.reservation_date_time);
        }
        catch (err) {
            return res.status(400).send("Invalid date format");
        }

        reservation.time_zone = timeZone.getTimezoneOffset();
    } 


    Reservation
        .findOneAndUpdate({ _id: req.params.id }, reservation)
        .exec(function (err, reservationOld) {
            if (err) return res.status(500).json({ err: err.message });

            res.json({ message: 'Successfully updated' })
        });
}

//GET requst to url/reservations/getByReservationsId/:id
exports.getByReservationId = async (req, res) => {
    const record = await Reservation.findOne({ _id: req.params.id });
    res.status(200).json(record);
}



//GET request to url/reservations/getByTerrainId/:id
exports.getByTerrainId = async (req, res) => {
    if (req.params.terrainId !== undefined) {
        const terrainId = req.params.terrainId;
        let records = await Reservation.find({ _terrain_id: terrainId });

        records = records.filter( r => r.reservation_date_time >= new Date());

        return res.status(200).json(records);
    }

    return res.status(400).send("Terrain Id cannot be undefined");
}

//GET request to url/reservations/getByUserId/:username
exports.getByUsername = async (req, res) => {
    const Username = req.params.username;
    const user = await User.findOne({ Username: Username });
    let records = await Reservation.find({ user_id: user._id }).exec();
    records = records.filter( r => r.reservation_date_time >= new Date());
    res.status(200).json(records);
}

exports.getByUserId = async (req, res) => {
    const userId = req.params.userId;
    const all = req.params.history;
    let records = await Reservation.find({ user_id: userId });
    if(all === 0)
        records = records.filter( r => r.reservation_date_time >= new Date());
    res.status(200).json(records);
}

//GET request to url/reservations/
exports.getAll = async (req, res) => {
    let records = await Reservation.find({});
    records = records.filter( r => r.reservation_date_time >= new Date());
    res.status(200).json(records);
}

async function overlapCheck(start, end, terrainId){

    var start = new Date(start);
    var end = new Date(end);

    //If end or begining of another reservation is inside the interval of start and end of current one
    const rs = await Reservation.find({_terrain_id: terrainId}).exec();
    let return_val = false;

    rs.forEach((r) => {
        var r_start = r.reservation_date_time;
        var r_end = r.end_date_time;

        if(start <= r_start && r_start < end){
            console.log(r_start);
            return_val = true;
            return ;
        }

        if(start < r_end && r_end <= end){
            return_val = true;
            return ;
        }

    })

    return return_val;
}