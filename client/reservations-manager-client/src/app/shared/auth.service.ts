import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  endpoint: string = environment.url;
  headers = new HttpHeaders().set('Content-Type', 'application-json');

  constructor(
    private http: HttpClient,
    public router: Router
  ) { }

  signIn(user: User) {
    return this.http.post<any>(`${this.endpoint}/auth/login`, user)
      .subscribe((res: any) => {
        localStorage.setItem('access_token', res.token);
        this.router.navigate(['home/setTermin']);
      },
      (error) => {
        window.alert("Greška: " + error.error);
        console.log(error);
      })
  }

  signUp(user: User): Observable<any> {
    let api = `${this.endpoint}/auth/register`;
    return this.http.post(api, user);
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');

    if(authToken === null || authToken == undefined){
      return false;
    }

    //ako je istekao token ukloni ga i vrati da korisnik nije ulogovan
    if(this.tokenExpired(authToken)){
      localStorage.removeItem('access_token');
      return false;
    }
    return true;
  }

  doLogout() {
    let removeToken = localStorage.removeItem('access_token');
    if (removeToken == null) {
      console.log("Something called logout");
      this.router.navigate(['signin']);
    }
  }


  handleError(error: HttpErrorResponse) {
    let msg = '';
    if (error.error instanceof ErrorEvent) {

      msg = error.error.message;
    } else {
      msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(msg);
  }

  private tokenExpired(token: string) {
    const expiry = (JSON.parse(atob(token.split('.')[1]))).exp;
    return (Math.floor((new Date).getTime() / 1000)) >= expiry;
  }

  //Inject auth controller into any component and call getUserClaimsRole to get the role of the current user
  //either user or admin
  public getUserClaimsRole(){
    let token = this.getToken();
    if(token == null){
      return 'norole';
    }

    return JSON.parse(atob(token.split('.')[1])).Role;
  }

  public getUserClaimsUserId(){
    let token = this.getToken();
    if(token == null){
      return 'nouser';
    }

    return JSON.parse(atob(token.split('.')[1])).user_id;
  }

  //get email the same way
  public getUserClaimsEmail(){
    let token = this.getToken();
    if(token == null){
      return 'noemail';
    }

    return JSON.parse(atob(token.split('.')[1])).Email;
  }
}

