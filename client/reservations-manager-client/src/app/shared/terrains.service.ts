import { Terrain } from './../models/terrain';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { postTerrain } from '../models/postTerrain';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TerrainsService {

  endpoint: string = environment.url;
  headers = new HttpHeaders().set('Content-Type', 'application-json');

  constructor(private http: HttpClient) { }

  getAllTerrains() {
    let path = this.endpoint + '/' + 'terrains' + '/' + 'getAll';

    return this.http.get<Terrain[]>(path);
  }

  getByTerrainId(id: string): Observable<Terrain> {
    let path = this.endpoint + '/' + 'terrains' + '/' + 'getByTerrainId/' + id;

    return this.http.get<Terrain>(path);
  }

  deleteTerrain(terrainId: string){
    let path = this.endpoint + '/' + 'terrains' + '/' + 'terrainDelete/' + terrainId;

    return this.http.delete<Terrain>(path);

  }

  postNewTerrain(ime: string, sport: string, cena: number) {

    var terrain = new postTerrain(ime, sport, cena);

    let path = this.endpoint + '/' + 'terrains' + '/' + 'terrainPost';

    return this.http.post(path, terrain);
  }

  putTerrain(terrain: Terrain){
    let path = this.endpoint + '/' + 'terrains' + '/' + 'terrainPut/' + terrain._id;

    return this.http.put(path, terrain);
  }

}
