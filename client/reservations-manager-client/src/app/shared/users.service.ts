import { User } from './../models/user';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  endpoint: string = environment.url;
  headers = new HttpHeaders().set('Content-Type', 'application-json');

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>("http://localhost:3000/users/");
  }

  putUser(user: User){

    let path = this.endpoint + '/' + 'users' + '/' + 'userPut/' + user._id;
    return this.http.put(path, user);

  }

  putUserPass(user: User, oldPass: string, newPass: string){

    let path = this.endpoint + '/' + 'users' + '/' + 'userPutPass/' + user._id + '/'  + oldPass + '/' + newPass;
    return this.http.put(path, user);

  }

  getMail(id: String, subject: String, body: String){

    let path = this.endpoint + '/users/email/' + id + '/' + subject + '/' + body;
    return this.http.get<String>(path);
  }
}
