export class Terrain{
  _id: string | undefined;
  Name: string | undefined;
  Sport: string | undefined;
  Price: number | undefined;

  constructor(_id: string, Name: string, Sport: string, Price: number){
    this._id = _id;
    this.Name = Name;
    this.Sport = Sport;
    this.Price = Price;
  }
}
