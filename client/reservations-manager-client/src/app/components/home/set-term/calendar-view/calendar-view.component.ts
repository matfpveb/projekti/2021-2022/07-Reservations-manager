import { HttpClient } from '@angular/common/http';
import {
  Component, OnInit, ChangeDetectionStrategy,
  ViewEncapsulation,
  Input,
  SimpleChanges
} from '@angular/core';

import {
  CalendarEvent,
  CalendarEventTimesChangedEvent,
  CalendarMonthViewDay,
  CalendarView,
} from 'angular-calendar';

import {
  subMonths,
  addMonths,
  addDays,
  addWeeks,
  addHours,
  subDays,
  subWeeks,
  startOfMonth,
  endOfMonth,
  startOfWeek,
  endOfWeek,
  startOfDay,
  endOfDay,
} from 'date-fns';
import { interval, Subscription, Observable, Subject} from 'rxjs';
import { Reservation } from 'src/app/models/reservation';
import { User } from 'src/app/models/user';
import { ReservationsService } from 'src/app/shared/reservations.service';
import { postReservation } from 'src/app/models/postReservation';
import { AuthService } from 'src/app/shared/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { TerrainsService } from 'src/app/shared/terrains.service';
import { UsersService } from 'src/app/shared/users.service';
import { Terrain } from 'src/app/models/terrain';

type CalendarPeriod = 'day' | 'week' | 'month';

type IdUserName = { user_id: string, username: string };

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

function addPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: addDays,
    week: addWeeks,
    month: addMonths,
  }[period](date, amount);
}

function subPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: subDays,
    week: subWeeks,
    month: subMonths,
  }[period](date, amount);
}

function startOfPeriod(period: CalendarPeriod, date: Date): Date {
  return {
    day: startOfDay,
    week: startOfWeek,
    month: startOfMonth,
  }[period](date);
}

function endOfPeriod(period: CalendarPeriod, date: Date): Date {
  return {
    day: endOfDay,
    week: endOfWeek,
    month: endOfMonth,
  }[period](date);
}

@Component({
  selector: 'app-calendar-view',
  changeDetection: ChangeDetectionStrategy.Default,
  templateUrl: './calendar-view.component.html',
  styleUrls: ['./calendar-view.component.css'],
  styles: [
    `
      .cal-disabled {
        background-color: #eee;
        pointer-events: none;
      }

      .cal-disabled .cal-day-number {
        opacity: 0.1;
      }
    `,
  ],
  encapsulation: ViewEncapsulation.None
})


export class CalendarViewComponent implements OnInit {

  view: CalendarView | CalendarPeriod = CalendarView.Month;

  viewDate: Date = new Date();

  //odavde potom treba ucitati termine iz baze
  events: CalendarEvent[] = [];

  minDate: Date = subDays(new Date(), 1);

  maxDate: Date = addWeeks(new Date(), 3);

  prevBtnDisabled: boolean = false;

  nextBtnDisabled: boolean = false;

  @Input() currentTerrainId: string;
  @Input() currentPrice: number;

  reservations: Reservation[];
  subscriptions: Subscription[];
  users: IdUserName[];
  //za admina vidi sve korisnike kad pravi rezervaciju
  Users: User[];

  postForm: FormGroup = this.fb.group({
    Duration: 0,
    usernameID: ""
  })

  constructor(public reservationsService: ReservationsService, public http: HttpClient, public authService: AuthService,
    public fb: FormBuilder, private terrainsService: TerrainsService,private usersService: UsersService) {
    this.dateOrViewChanged();
    this.subscriptions = [];
    if(authService.getUserClaimsRole() === 'admin'){
      this.getUsersObject();
    }
  }


  ngOnInit(): void {

  }

  //Monitoring for @Input value change
  ngOnChanges(changes: SimpleChanges) {

    this.fetchReservations(changes['currentTerrainId'].currentValue);
  }

  getAllUsers(): Observable<User[]> {
    return this.http.get<User[]>("http://localhost:3000/users/");
  }

  getUsersObject(){
    this.subscriptions.push(
      this.usersService.getAllUsers().subscribe(
        (res) => {
          this.Users = res;
        }
      )
    )
  }

  fetchReservations(terrainId: string): void {
    this.currentTerrainId = terrainId;
    this.reservations = [];

    if (this.currentTerrainId !== undefined) {
      if (this.currentTerrainId != "Izaberite teren") {
        this.subscriptions.push(this.reservationsService.getByTerrainId(this.currentTerrainId).subscribe(
          (res) => {
            this.reservations = res;
            this.showReservations();
          },
          (error) => {
            window.alert("Error fetching reservations by terrain id");
          }
        ));
      }
      else{
        this.events = [];
      }
    }
  }

  showReservations() {
    this.events = [];

    this.reservations.forEach((r) => {
      this.events = [
        ...this.events,
        {
          start: addHours(new Date(r.reservation_date_time), 0),
          end: addHours(new Date(r.reservation_date_time), r.duration),
          title: r.username,
          color: colors.blue
        }
      ];
    });

  }

  submitReservation(){
    //getUserClaimsUserId
    if(this.changeDate === null){
      return ;
    }
    
    var dur = this.postForm.get('Duration').value;
   
    if (this.authService.getUserClaimsRole() === 'admin') {
      var userNameAdminID = this.postForm.get('usernameID').value;   
      this.subscriptions.push(this.http.get<string>(`http://localhost:3000/users/${userNameAdminID}`).subscribe(
        (u) => {
          this.subscriptions.push(
            this.reservationsService.postNewReservation(this.clickedDate, u, this.currentTerrainId, dur).subscribe(
              (res) => {
                //fetch again
                this.fetchReservations(this.currentTerrainId);
  
                window.alert("Termin je uspešno zakazan");
                this.postForm.reset();
              },
              (err) => {
                window.alert("Error adding a new reservation: " + err.error);
                console.log(err.error);
              }
            )
          )
        }
      ));
    } else {
      this.subscriptions.push(this.http.get<string>(`http://localhost:3000/users/${this.authService.getUserClaimsUserId()}`).subscribe(
        (u) => {
          this.subscriptions.push(
            this.reservationsService.postNewReservation(this.clickedDate, u, this.currentTerrainId, dur).subscribe(
              (res) => {
              //fetch again
                this.fetchReservations(this.currentTerrainId);

                window.alert("Termin je uspesno zakazan");
                this.postForm.reset();
              },
              (err) => {
                window.alert("Error adding a new reservation: " + err.error);
                console.log(err.error);
              }
            )
          )
        }
    ));
  }

    ;
  }

  increment(): void {
    this.changeDate(addPeriod(this.view, this.viewDate, 1));
  }

  decrement(): void {
    this.changeDate(subPeriod(this.view, this.viewDate, 1));
  }

  today(): void {
    this.changeDate(new Date());
  }

  dateIsValid(date: Date): boolean {
    return date >= this.minDate && date <= this.maxDate;
  }

  changeDate(date: Date): void {
    this.viewDate = date;
    this.dateOrViewChanged();
  }

  changeView(view: CalendarPeriod): void {
    this.view = view;
    this.dateOrViewChanged();
  }

  dateOrViewChanged(): void {
    this.prevBtnDisabled = !this.dateIsValid(
      endOfPeriod(this.view, subPeriod(this.view, this.viewDate, 1))
    );
    this.nextBtnDisabled = !this.dateIsValid(
      startOfPeriod(this.view, addPeriod(this.view, this.viewDate, 1))
    );
    if (this.viewDate < this.minDate) {
      this.changeDate(this.minDate);
    } else if (this.viewDate > this.maxDate) {
      this.changeDate(this.maxDate);
    }
  }
  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {
    body.forEach((day) => {
      if (!this.dateIsValid(day.date)) {
        day.cssClass = 'cal-disabled';
      }
    });
  }

  clickedDate: Date;

  clickedColumn: number;
  refresh = new Subject<void>();

  clickedFieldEvent(event) {
      this.clickedDate = event.day.date;
  }

  ngOnDestroy() {
    this.subscriptions.forEach((s) => s.unsubscribe());
  }

}
