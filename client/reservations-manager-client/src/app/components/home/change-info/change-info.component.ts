import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from 'src/app/shared/auth.service';
import { HttpClient } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { environment } from 'src/environments/environment';
import { ReservationsService } from 'src/app/shared/reservations.service';
import { Reservation } from 'src/app/models/reservation';
import { UsersService } from 'src/app/shared/users.service';

@Component({
  selector: 'app-change-info',
  templateUrl: './change-info.component.html',
  styleUrls: ['./change-info.component.css']
})
export class ChangeInfoComponent implements OnInit {

  userForma = this.fb.group({
    Username: [''],
    Firstname: [''],
    Lastname: [''],
    Email: ['']
  })

  passForma = this.fb.group({
    OldPass: [''],
    NewPass: [''],
    NewPass1: ['']
  })

  user: User;
  email: string;
  endpoint: string = environment.url;
  Reservations: Reservation[];
  subscriptions: Subscription[];

  constructor(private authService: AuthService,  private fb: FormBuilder,
    public ngxSmartModalService: NgxSmartModalService, private http: HttpClient,
     private reservationsService: ReservationsService, private usersService: UsersService) {

    this.subscriptions = [];
    this.Reservations = [];
    this.email = this.authService.getUserClaimsEmail();

   }

  ngOnInit(): void {
    this.getUserObject();
  }



  getUserObject(){
    this.subscriptions.push(
      this.usersService.getAllUsers().subscribe(
        (res) => {
          this.user = res.find(element => element.Email === this.authService.getUserClaimsEmail());
          this.setReservations();
        }
      )
    )
  }

  izmeniPass(){

    this.passForma.setValue({
      OldPass: [''],
      NewPass: [''],
      NewPass1: ['']
    });

    this.ngxSmartModalService.getModal('myModal2').open();

  }

  izmeniUser(){
    this.userForma.setValue({
      Username: this.user.Username,
      Firstname: this.user.FirstName,
      Lastname: this.user.LastName,
      Email: this.user.Email
    })


    this.ngxSmartModalService.getModal('myModal').open();

  }

  setReservations(){
    this.subscriptions.push(
      this.reservationsService.getByUserId(this.user._id, true).subscribe(
        (res) => {
          this.Reservations = res;
        }
      )
    )
  }

  izmeni(){

    var Username = this.userForma.get("Username").value;
    var Firstname = this.userForma.get("Firstname").value;
    var Lastname = this.userForma.get("Lastname").value;
    var Email = this.userForma.get("Email").value;

    this.user.Username = Username;
    this.user.FirstName = Firstname;
    this.user.LastName = Lastname;
    this.user.Email = Email;


    this.subscriptions.push(
      this.usersService.putUser(this.user).subscribe(
        (res) => {
          this.refreshUsers();
        },
        (error) => {
          window.alert("Došlo je do greške: " + error.err);
        }
      )
    )

    this.Reservations.forEach(element => {

        element.username = Username;

        this.subscriptions.push(
          this.reservationsService.putReservation(element).subscribe(
            (res) => {

            },
            (error) => {
              window.alert("Došlo je do greške: " + error.err);
            }
          )
        )
    });

    this.ngxSmartModalService.getModal('myModal').close();
  }

  izmeniPassword(){


    var OldPass = this.passForma.get("OldPass").value;
    var NewPass = this.passForma.get("NewPass").value;
    var NewPass1 = this.passForma.get("NewPass1").value;



    this.subscriptions.push(
      this.usersService.putUserPass(this.user, OldPass, NewPass).subscribe(
        (res) => {
          this.refreshUsers();
          this.ngxSmartModalService.getModal('myModal2').close();
        },
        (error) => {
          window.alert("Pogresna stara šifra");
        }
      )
    )

    if(NewPass != NewPass1){
      window.alert("Ne podudaraju se šifre");
      return;
    }

  }


  refreshUsers() {
    this.subscriptions.push(

      this.usersService.getAllUsers().subscribe(
        (res) => {
          this.user = res.find(element => element.Email === this.authService.getUserClaimsEmail());
        }
      )

    )
  }

}
