import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Subscription } from 'rxjs';
import { Reservation } from 'src/app/models/reservation';
import { Terrain } from 'src/app/models/terrain';
import { AuthService } from 'src/app/shared/auth.service';
import { ReservationsService } from 'src/app/shared/reservations.service';
import { TerrainsService } from 'src/app/shared/terrains.service';

@Component({
  selector: 'app-cancel-term',
  templateUrl: './cancel-term.component.html',
  styleUrls: ['./cancel-term.component.css']
})
export class CancelTermComponent implements OnInit {

  reservations: Reservation[];
  terrainIdToName: any = {};

  subscriptions: Subscription[];

  constructor(private reservationsService: ReservationsService,
    private authService: AuthService, private terrainService: TerrainsService) {
    this.subscriptions = [];

  }

  ngOnInit(): void {
    var str = this.authService.getUserClaimsUserId();
    this.subscriptions.push(this.reservationsService.getByUserId(
      str, false
    ).subscribe(
      (res) => {
        this.reservations = res;
        this.getTerrainNames();
      },
      (error) => {
        window.alert("Error fetching reservations");
      }
    )
    );
  }

  getTerrainNames(){

    this.reservations.forEach(x => {
      if(!(x._terrain_id in this.terrainIdToName)){
        this.subscriptions.push(
          this.terrainService.getByTerrainId(x._terrain_id).subscribe(
            (res) => {
              this.terrainIdToName[x._terrain_id] = res.Name;
            }
          )
        )
      }
    })

  }

  removeReservation(id: string) {
    if (confirm("Da li ste sigurni da želite da otkažete termin")) {

      this.subscriptions.push(
        this.reservationsService.deleteReservation(id).subscribe(
          (res) => {
            this.reservations = this.reservations.filter(x => x._id != id);
          }
        )
      )
    }
  }

  dateToLocalString(date: Date) {
    var tmp : string = moment(date).locale('sr').format("dddd, MMMM Do YYYY, h:mm:ss a");
    return tmp.substring(0, tmp.length-6) + tmp.substring(tmp.length-3);
  }


  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
