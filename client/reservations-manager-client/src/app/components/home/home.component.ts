import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/shared/auth.service';
import { ReservationsService } from 'src/app/shared/reservations.service';

import * as moment from 'moment';
import { postReservation } from 'src/app/models/postReservation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

// unsubscibe all subscriptions

export class HomeComponent implements OnInit {

  constructor(public authService: AuthService,
    public reservationService : ReservationsService) {

    }

  ngOnInit(): void {
  }

  logOut():void{
    if(confirm("Da li ste sigurni da želite da se izlogujete")){
      this.authService.doLogout();
    }
  }

}
