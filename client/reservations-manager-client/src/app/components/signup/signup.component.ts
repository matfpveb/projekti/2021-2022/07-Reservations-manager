import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription, windowWhen } from 'rxjs';
import { AuthService } from 'src/app/shared/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  signUpSubscription: Subscription | null;

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router
  ) {
    this.signupForm = this.fb.group({
      Username: [''],
      FirstName:[''],
      LastName: [''],
      Email: [''],
      Password: ['']
    })

    this.signUpSubscription = null;
  }

  

  ngOnInit() { }

  registerUser() {
    
    this.signUpSubscription = this.authService.signUp(this.signupForm.value).subscribe((res) => {
      if (res != null) {
        this.signupForm.reset();
        localStorage.setItem('access_token', res.token);
        this.router.navigate(['home/setTermin']);
      }
    },
    (error) => {
      window.alert("Greška pri prijavljivanju " + error.error);
    })
  }

  navigateToSignIn(){
    this.router.navigate(['signin']);
  }

  ngOnDestroy(){
    if(this.signUpSubscription !== null)
    {
      this.signUpSubscription.unsubscribe();
    }
  }

}